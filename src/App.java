import java.util.ArrayList;
import java.util.Iterator;

public class App {
    public static void main(String[] args) throws Exception {
        // task 1: Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra
        // terminal ArrayList vừa tạo
        ArrayList<String> task1 = new ArrayList<>();
        task1.add("red");
        task1.add("pink");
        task1.add("blue");
        task1.add("white");
        task1.add("black");
        System.out.println(task1);

        //task 2: Tạo mới 2 ArrayList Integer, thêm 3 số (kiểu int) vào mỗi ArrayList, cộng các giá trị của một ArrayList vào ArrayList còn lại. Ghi ra terminal ArrayList được cộng thêm.
        ArrayList<Integer> list1 = new ArrayList<Integer>();
        list1.add(1);
        list1.add(2);
        list1.add(3);

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        list2.add(4);
        list2.add(5);
        list2.add(6);

        for( int i = 0; i < list1.size(); i++){
            int sum = list1.get(i) + list2.get(i);
            list1.set(i, sum);
        }
        System.out.println("Task 2: " + list1);

        // task 3: Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra terminal số lượng phần tử của ArrayList vừa tạo
        System.out.println("Task 3: " + task1.size());

        // task 4: Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String) và ghi ra terminal phần tử thứ 4 của ArrayList vừa tạo
        System.out.println("Task 4: " + task1.get(4));

        // task 5: Tạo mới 1 ArrayList String, thêm tối đa 5 màu sắc (kiểu String) và ghi ra terminal phần tử cuối cùng của ArrayList vừa tạo (Không sử dụng index trực tiếp)
        System.out.println("Task 5: " + task1.get(task1.size() -1));

        // task 6: Tạo mới 1 ArrayList String, thêm tối đa 5 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Xóa phần tử cuối cùng của ArrayList trên và ghi lại ra terminal ArrayList vừa tạo.
        task1.remove(task1.size() -1);
        System.out.println("Task 5: Xóa phần tử cuối cùng của ArrayList " + task1);
        // task 7: Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String). Sử dụng forEach ghi ra terminal giá trị từng phần tử của ArrayList vừa tạo
        task1.forEach(t -> System.out.print(t + ", "));
        System.out.println();
        // task 8: Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String). Sử dụng iterator ghi ra terminal giá trị từng phần tử của ArrayList vừa tạo
        System.out.println("Task 8");

        Iterator<String> itr =  task1.iterator();
        while(itr.hasNext()){
            String element = itr.next();
            System.out.print(element + ", ");
        }
        System.out.println();
        // task 9: Tạo mới 1 ArrayList String, thêm 5 màu sắc (kiểu String). Sử dụng vòng lặp for ghi ra terminal giá trị từng phần tử của ArrayList vừa tạo
        System.out.println("Task 9:");
        for(String s : task1){
            System.out.print(s + ", ");
        }

        // task 10: Tạo mới 1 ArrayList String, thêm tối đa 5 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Thêm một màu sắc vào đầu của ArrayList trên và ghi lại ra terminal ArrayList vừa tạo.

        task1.add(0, "black");
        System.out.println(task1);

        // task 11: Tạo mới 1 ArrayList String, thêm tối đa 5 màu sắc (kiểu String) và ghi ra terminal ArrayList vừa tạo. Sửa màu của phần tử thứ 3 của ArrayList trên thành màu vàng và ghi lại ra terminal ArrayList vừa tạo.
        task1.set(2, "yellow");
         System.out.println(task1);

         //task 12: Tạo mới 1 ArrayList String, thêm lần lượt các giá trị John, Alice, Bob, Steve, John, Steve, Maria vào ArrayList vừa tạo. Ghi ra terminal vị trí đầu tiên của 2 phần tử có giá Alice và Mark trong ArrayList trên.
        ArrayList<String> list = new ArrayList<>();
        list.add("John");
        list.add("Alice");
        list.add("Bob");
        list.add("Steve");
        list.add("John");
        list.add("Steve");
        list.add("Maria");

        int aliceIndex = list.indexOf("Alice");
        int markIndex = list.indexOf("Mark");

        System.out.println("Vi tri đau tien cua Alice: " + aliceIndex);
        System.out.println("Vi tri đau tien cua Mark: " + markIndex);
        //task 13: Ghi ra terminal vị trí cuối cùng xuất hiện của 2 phần tử có giá Steve và John trong ArrayList trên.
        int steveIndex = list.lastIndexOf("Steve");
        int johnIndex = list.lastIndexOf("John");

        System.out.println("Vi tri cuoi cung xuat hien cua Steve: " + steveIndex);
        System.out.println("Vi tri cuoi cung xuat hien cua John: " + johnIndex);

    }
}
